<?php
namespace Controllers;

use Models\Languages;
use Models\Versions;
use Phalcon\Http\Response;
use Models\Insurers;
use Models\Cities;
use Models\Countries;

class GetController extends BaseController {
    public function insurers() {
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if ($this->hasSession('auth')) {
            $insurers = $this->getCache('insurersAll');
            if ($insurers === null) {
                $insurers = Insurers::find(
                    array(
                        "order" => "id",
                    )
                );

                $this->saveCache('insurersAll', $insurers);
            }

            if (is_object($insurers) && $insurers->count() >= 0) {
                $result = array();

                foreach ($insurers as $insurer) {
                    $result[] = array(
                        "name" => $insurer->getName(),
                        "id" => $insurer->getId()
                    );
                }

                $response->setStatusCode(200, 'OK');
                $response->setContent(json_encode($result, JSON_HEX_APOS | JSON_HEX_QUOT));

                return $response;
            } else {
                return $this->hasFailed($response, 400);
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }

    public function cities() {
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if ($this->hasSession('auth')) {
            $cities = $this->getCache('citiesAll');
            if ($cities === null) {
                $cities = Cities::find(
                    array(
                        "order" => "name",
                    )
                );

                $this->saveCache('citiesAll', $cities);
            }

            if (is_object($cities) && $cities->count() >= 0) {
                $result = array();

                foreach ($cities as $city) {
                    $result[] = array(
                        "name" => $city->getName()
                    );
                }

                $response->setStatusCode(200, 'OK');
                $response->setContent(json_encode($result, JSON_HEX_APOS | JSON_HEX_QUOT));

                return $response;
            } else {
                return $this->hasFailed($response, 400);
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }

    public function countries() {
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if ($this->hasSession('auth')) {
            $countries = $this->getCache('countriesAll');
            if ($countries === null) {
                $countries = Countries::find(
                    array(
                        "order" => "name",
                    )
                );

                $this->saveCache('countriesAll', $countries);
            }

            if (is_object($countries) && $countries->count() >= 0) {
                $result = array();

                foreach ($countries as $country) {
                    $result[] = array(
                        "name" => $country->getName(),
                        "code" => $country->getCode()
                    );
                }

                $response->setStatusCode(200, 'OK');
                $response->setContent(json_encode($result, JSON_HEX_APOS | JSON_HEX_QUOT));

                return $response;
            } else {
                return $this->hasFailed($response, 400);
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }

    public function languages() {
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if ($this->hasSession('auth')) {
            $languages = $this->getCache('languagesAll');
            if ($languages === null) {
                $languages = Languages::find(
                    array(
                        "order" => "name",
                    )
                );

                $this->saveCache('languagesAll', $languages);
            }

            if (is_object($languages) && $languages->count() >= 0) {
                $result = array();

                foreach ($languages as $language) {
                    $result[] = array(
                        "name" => $language->getName(),
                        "code" => $language->getCode()
                    );
                }

                $response->setStatusCode(200, "OK");
                $response->setContent(json_encode($result, JSON_HEX_APOS | JSON_HEX_QUOT));

                return $response;
            } else {
                return $this->hasFailed($response, 400);
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }

    public function version() {
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if ($this->hasSession('auth')) {
            $version = $this->getCache('versionsAll');
            if ($version === null) {
                $version = Versions::findFirst();

                $this->saveCache('versionsAll', $version);
            }

            if (is_object($version) && $version->count() >= 0) {
                $result = array(
                    "api" => $version->getApi(),
                    "cities" => $version->getCities(),
                    "countries" => $version->getCountries(),
                    "languages" => $version->getLanguages(),
                    "insurers" => $version->getInsurers()
                );

                $response->setStatusCode(200, "OK");
                $response->setContent(json_encode($result, JSON_HEX_APOS | JSON_HEX_QUOT));

                return $response;
            } else {
                return $this->hasFailed($response, 400);
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }
}