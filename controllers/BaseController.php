<?php
namespace Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\DI;

/**
 * Class BaseController
 *      Zostawiam Ci tu kilka metod, ktore moga sie przydac podczas rozwoju
 *      wszystkie klasy controllers dziedzicza po tej klasie
 * @package Controllers
 */
class BaseController extends Controller {
    public $status400msg = "Something went wrong - try again";

    private $cache = null;

    /**
     * @object $response - response object
     * @int $status - eg 404
     * @string null $message
     * @object mixed
     */
    public function hasFailed($response, $status, $message = null) {
        $description = array(
            304 => "Not Modified",
            400 => "Bad Request",
            401 => "Unauthorized",
            404 => "Not Found",
            501 => "Internal Server Error"
        );

        $content = isset($message) ? $message : $this->status400msg;

        $response->setStatusCode($status, $description[$status]);
        $response->setContent($content);

        return $response;
    }

    public function beforeHandleRoute() {
        'The main method is just called, at this point the application doesn’t know if there is some matched route';
    }

    public function beforeExecuteRoute() {
        'A route has been matched and it contains a valid handler, at this point the handler has not been executed';
    }

    public function afterExecuteRoute() {
        'Triggered after running the handler';
    }

    public function beforeNotFound() {
        'Triggered when any of the defined routes match the requested URI';
    }

    public function afterHandleRoute() {
        'Triggered after completing the whole process in a successful way';
    }

    private function setCacheConfiguration() {
        $this->cache = DI::getDefault()->getShared('modelsCache');
    }

    private function getCacheConfiguration() {
        if (!isset($this->cache)) {
            $this->setCacheConfiguration();
        }
        
        return $this->cache;
    }

    protected function getCache($name) {
        $cache = $this->getCacheConfiguration();
        $name .= '_' + $this->getSession('auth');

        return $cache->get($name);
    }

    protected function saveCache($name, $insurers) {
        $cache = $this->getCacheConfiguration();
        $name .= '_' + $this->getSession('auth');

        $cache->save($name, $insurers);
    }

    protected function startSession($key, $userId) {
        $this->session->set($key, $userId);
    }

    protected function hasSession($key) {
        return $this->session->has($key);
    }

    protected function getSession($key) {
        return $this->session->get($key);
    }
}