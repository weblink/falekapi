<?php
namespace Controllers;

use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Models\Users;
use Models\Incidents;
use Models\RequestCodes;

class PostController extends BaseController {
    public function login() {
        $request = new Request();
        $data = $request->getJsonRawBody();

        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if (empty($data)) {
            return $this->setFailed($response, 400, "Brak danych");
        }

        if (isset($data->password)) {
            $login = $this->loginWithPassword($data);
        } else {
            $login = $this->loginWithPIN($data);
        }

        if ($login['status'] === true) {
            $response->setStatusCode(200, "OK");
            $response->setContent(json_encode($login['object'], JSON_HEX_APOS | JSON_HEX_QUOT));
            $this->startSession('auth', $login['userId']);

            return $response;
        } else {
            return $this->hasFailed($response, $login['errorNo'], $login['error']);
        }
    }

    public function requestCode() {
        $request = new Request();
        $data = $request->getJsonRawBody();
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        $time = time();
        $expiredTime = $time + 5*60;
        $pin = $this->generatePIN();

        if (empty($data)) {
            return $this->hasFailed($response, 400, "Brak danych");
        }

        $codes = new RequestCodes();
        $codes->setPhoneNumber($data->phone_number);
        $codes->setPin($pin);
        $codes->setExpiredTime(date("Y-m-d H:i:s", $expiredTime));

        if ($codes->save() === false) {
            return $this->hasFailed($response, 501);
        } else {
            $result = array("pin" => $pin);
            $response->setStatusCode(200, "OK");
            $response->setContent(json_encode($result, JSON_HEX_APOS | JSON_HEX_QUOT));

            return $response;
        }
    }

    public function incident() {
        $request = new Request();
        $data = $request->getJsonRawBody();

        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if ($this->hasSession('auth')) {
            if (empty($data)) {
                return $this->hasFailed($response, 400, "Brak danych");
            }

            $incident = new Incidents();
            $incident->setDate($data->date);
            $incident->setSentInsurer((int)$data->sent_insurer);
            $incident->setSentRelatives((int)$data->sent_relatives);
            $incident->setThreatOfLife((int)$data->threat_of_life);

            if (isset($data->care_plate_number)) {
                $incident->setCarPlateNumber($data->care_plate_number);
            }

            if (isset($data->car_brand)) {
                $incident->setCarBrand($data->car_brand);
            }

            if (isset($data->car_model)) {
                $incident->setCarModel($data->car_model);
            }

            if ($incident->save() === false) {
                return $this->hasFailed($response, 501);
            } else {
                $response->setStatusCode(201, "Created");

                return $response;
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }

    private function generatePIN() {
        $result = '';
        for ($i = 0; $i < 8; $i++) {
            $result .= rand(0,9);
        }

        return (int) $result;
    }

    private function loginWithPIN($data) {
        $phone = isset($data->phone_number) ? $data->phone_number : null;
        $pin = isset($data->pin) ? $data->pin : null;

        $codesAmount = RequestCodes::find(
            array(
                "conditions" => "phone_number = :number:",
                "bind" => array(
                    "number" => $phone
                ),
                "order" => "expired_time DESC",
                "limit" => 1
            )
        );

        if (is_object($codesAmount)) {
            if ($codesAmount->count() === 1) {
                foreach ($codesAmount as $code) {
                    $client = $code;
                }

                if ($pin === $client->getPin()) {
                    RequestCodes::find(
                        array(
                            "conditions" => "phone_number = :number:",
                            "bind" => array(
                                "number" => $phone
                            )
                        )
                    )->delete();

                    $usersEntry = Users::find(
                        array(
                            "conditions" => "phone_number = :number:",
                            "bind" => array(
                                "number" => $phone
                            )
                        )
                    );

                    if ($usersEntry->count() === 0) {
                        $user = new Users();
                        $user->setPhoneNumber($phone);
                        $user->setInsurerId(0);
                        $user->save();
                        $userId = $user->getId();
                    } else {
                        $userId = $usersEntry->getId();
                    }

                    return array(
                        "status" => true,
                        "userId" => $userId,
                        "object" => array(
                            "user" => array()
                        )
                    );
                } else {
                    return array(
                        "status" => false,
                        "errorNo" => 400,
                        "error" => "Zły PIN"
                    );
                }
            } else {
                return array(
                    "status" => false,
                    "errorNo" => 400,
                    "error" => "Brak kodu pin"
                );
            }
        } else {
            return array(
                "status" => false,
                "errorNo" => 500,
                "error" => "Problem z pobraniem danych"
            );
        }
    }

    private function loginWithPassword($data) {
        $phone = isset($data->phone_number) ? $data->phone_number : null;
        $password = isset($data->password) ? $data->password : null;

        $users = Users::find(
            array(
                "conditions" => "phone_number = :number:",
                "bind" => array(
                    "number" => $phone
                ),
                "limit" => 1
            )
        );

        if (is_object($users)) {
            if ($users->count() === 1) {
                foreach ($users as $user) {
                    $client = $user;
                }

                if ($password === $client->getPassword()) {
                    $result = array(
                        "status" => true,
                        "userId" => $client->getId(),
                        "object" => array(
                            "user" => array(
                                "name" => $client->getName(),
                                "lastname" => $client->getLastname(),
                                "car_plate_number" => $client->getCarPlateNumber(),
                                "email" => $client->getEmail(),
                                "important_data" => $client->getImportantData(),
                                "address" => array(
                                    "city" => $client->getCity(),
                                    "post_code" => $client->getPostCode(),
                                    "country" => $client->getCountry(),
                                    "street" => $client->getStreet()
                                ),
                                "settings" => array(
                                    "inform_relatives" => $client->getInformRelatives(),
                                    "inform_insurer" => $client->getInformInsurer()
                                )
                            )
                        )
                    );

                    if ($client->insurers->count() > 0) {
                        $result['object']['user']['insurer'] = array(
                            "insurer_id" => $client->getInsurerId(),
                            "policy_number" => $client->getPolicyNumber()
                        );
                    }

                    if ($client->relatives->count() > 0) {
                        $i = 0;
                        foreach ($client->relatives as $relative) {
                            $result['object']['user']['relatives'][] = array(
                                "name" => $relative->getName(),
                                "phone" => $relative->getPhoneNumber()
                            );

                            $i++;
                            if ($i === 3) {
                                break;
                            }
                        }
                    }
                } else {
                    return array(
                        "status" => false,
                        "errorNo" => 400,
                        "error" => "Złe hasło"
                    );
                }
            } else {
                return array(
                    "status" => false,
                    "errorNo" => 400,
                    "error" => "Brak konta dla podanego numeru"
                );
            }
        } else {
            return array(
                "status" => false,
                "errorNo" => 500,
                "error" => "Problem z pobraniem danych"
            );
        }

        return $result;
    }
}