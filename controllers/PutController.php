<?php
namespace Controllers;

use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Models\Localizations;

class PutController extends BaseController {
    public function localization() {
        $request = new Request();
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        if ($this->hasSession('auth')) {
            $data = $request->getJsonRawBody();

            if (empty($data)) {
                return $this->hasFailed($response, 400, "Brak danych");
            }

            $status = true;

            foreach ($data as $option) {
                $localization = new Localizations();
                $localization->setAccuracy((int) $option->accuracy);
                $localization->setLatitude($option->latitude);
                $localization->setLongitude($option->longitude);
                $localization->setProvider($option->provider);
                $localization->setDate($option->date);

                if ($localization->save() === false) {
                    $status = false;
                }
                unset($localization);
            }

            if ($status === true) {
                $response->setStatusCode(201, "Created");

                return $response;
            } else {
                return $this->hasFailed($response, 501);
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }
}