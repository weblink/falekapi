<?php
namespace Controllers;

use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Models\Users;

class UpdateController extends BaseController {
    public function user() {
        $request = new Request();
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');

        $data = $request->getJsonRawBody();

        if ($this->hasSession('auth')) {
            if (empty($data)) {
                return $this->hasFailed($response, 400, "Brak danych");
            }

            $mainUpdate = array(
                "name" => isset($data->user->name) ? $data->user->name : null,
                "lastname" => isset($data->user->lastname) ? $data->user->lastname : null,
                "car_plate_number" => isset($data->user->car_plate_number) ? $data->user->car_plate_number : null,
                "email" => isset($data->user->email) ? $data->user->email : null,
                "important_data" => isset($data->user->important_data) ? $data->user->important_data : null
            );

            $mainUpdate = array_filter($mainUpdate);

            $addressUpdate = array(
                "city" => isset($data->user->address->city) ? $data->user->address->city : null,
                "post_code" => isset($data->user->address->post_code) ? $data->user->address->post_code : null,
                "country" => isset($data->user->address->country) ? $data->user->adress->country : null,
                "street" => isset($data->user->address->street) ? $data->user->address->street : null
            );

            $addressUpdate = array_filter($addressUpdate);

            $settingsUpdate = array(
                "inform_relatives" => isset($data->user->settings->inform_relatives) ? $data->user->settings->inform_relatives : null,
                "inform_insurer" => isset($data->user->settings->inform_insurer) ? $data->user->settings->inform_insurer : null
            );

            $settingsUpdate = array_filter($settingsUpdate);

            $insurerUpdate = array(
                "insurer_id" => isset($data->user->insurer->insurer_id) ? $data->user->insurer->insurer_id : null,
                "policy_number" => isset($data->user->insurer->policy_number) ? $data->user->insurer->policy_number : null
            );

            $insurerUpdate = array_filter($insurerUpdate);

            $update = array();
            if (count($mainUpdate) > 0) {
                $update = $mainUpdate;
            }

            if (count($addressUpdate) > 0) {
                $update['address'] = $addressUpdate;
            }

            if (count($settingsUpdate)) {
                $update['settings'] = $settingsUpdate;
            }

            if (count($insurerUpdate) > 0) {
                $update['insurer'] = $insurerUpdate;
            }

            $user = $this->getCache('userFirst');
            if ($user === null) {
                $user = Users::findFirst(
                    array(
                        "conditions" => "id = :id:",
                        "bind" => array(
                            "id" => 1 // w przyszlosci podmiana na id z ciastka
                        )
                    )
                );

                $this->saveCache('userFirst', $user);
            }

            if ($user->update($update) === false) {
                return $this->hasFailed($response, 400);
            } else {
                $response->setStatusCode(201, "Created");

                return $response;
            }
        } else {
            return $this->hasFailed($response, 401);
        }
    }
}