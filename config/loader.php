<?php
use Phalcon\Loader;

/**
 * Registering an autoloader
 */
$loader = new Loader();

/**
 * Loading structure files directions
 */
$loader->registerDirs(array(
    __DIR__ . '/cron/'
));

/**
 * Loading class directions
 */
$loader->registerNamespaces(array(
    'Controllers' => $config->application->controllersDir,
    'Collections' => $config->application->collectionsDir,
    'Models' => $config->application->modelsDir
));

$loader->register();