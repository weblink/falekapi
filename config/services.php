<?php
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Url as Url;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;
use Phalcon\Cache\Frontend\Data as FrontendData;
use Phalcon\Cache\Backend\Memcache as BackendMemcache;
use Phalcon\Session\Adapter\Files as Session;

$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function() use ($config) {
    $url = new Url();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($config) {
    return new Database (array(
        "host" => $config->database->host,
        "username" => $config->database->user,
        "password" => $config->database->password,
        "dbname" => $config->database->db
    ));
});

$di->set('modelsCache', function () {

    // Cache data for one minute by default
    $frontCache = new FrontendData(
        array(
            "lifetime" => 60*60
        )
    );

    // Memcached connection settings
    $cache = new BackendMemcache(
        $frontCache,
        array(
            "host" => "localhost",
            "port" => "11211"
        )
    );

    return $cache;
});

$di->setShared('session', function () {
    $session = new Session();
    $session->start();

    return $session;
});