<?php

return new \Phalcon\Config(array(
	'database' => array(
		'host' => 'localhost',
		'user' => 'www-data',
		'password' => 'apiDlaFalka',
		'db' => 'api'
	),
	'application' => array(
		'modelsDir' => __DIR__ . '/../models/',
		'controllersDir' => __DIR__ . '/../controllers',
		'collectionsDir' => __DIR__ . '/../collections',
		'cronDir' => __DIR__ . '/../cron',
		'baseUri'        => '/',
	),
	'models' => array(
		'metadata' => array(
			'adapter' => 'Memory'
		)
	)
));

