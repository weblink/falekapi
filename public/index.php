<?php

error_reporting(E_ALL);

use Phalcon\Mvc\Micro;
use Phalcon\Http\Response;

try {
	/**
	 * Read the configuration
	 */
	$config = include __DIR__.'/../config/config.php';

	include __DIR__. '/../config/services.php';

	include __DIR__. '/../config/loader.php';

	$app = new Micro($di);

	$get = new Collections\GetCollection();
	$post = new Collections\PostCollection();
	$put = new Collections\PutCollection();
	$update = new Collections\UpdateCollection();

	$app->mount($get->registerCollection());
	$app->mount($post->registerCollection());
	$app->mount($put->registerCollection());
	$app->mount($update->registerCollection());

	/**
	 * Add your routes here
	 */
	$app->get('/', function () {
		echo '<h1>API</h1>';
		echo '<h2>/get/</h2>';
		echo '<h2>/post/</h2>';
		echo '<h2>/put/</h2>';
		echo '<h2>/update/</h2>';
	});

	/**
	 * Not found handler
	 */
	$app->notFound(function () {
		$response = new Response();
		$response->setStatusCode(404, "Not found");
		$response->setContent("This is crazy, but this page was not found!");

		return $response;
	});

	/**
	 * Handle the request
	 */
	$app->handle();

} catch (\Exception $e) {
	echo $e->getMessage(), PHP_EOL;
	echo $e->getTraceAsString();
}