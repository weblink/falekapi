<?php
namespace Collections;

use Controllers\UpdateController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

class UpdateCollection extends BaseCollection {
    public function registerCollection() {
        $update = new MicroCollection();
        $update->setHandler(new UpdateController());
        $update->setPrefix('/update');
        $update->patch('/user', 'user');

        return $update;
    }
}