<?php
namespace Collections;

use Controllers\PutController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

class PutCollection extends BaseCollection {
    public function registerCollection() {
        $put = new MicroCollection();
        $put->setHandler(new PutController());
        $put->setPrefix('/put');
        $put->put('/localization', 'localization');

        return $put;
    }
}