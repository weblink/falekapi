<?php
namespace Collections;

use Controllers\GetController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

class GetCollection extends BaseCollection {
    public function registerCollection() {
        $get = new MicroCollection();
        $get->setHandler(new GetController());
        $get->setPrefix('/get');
        $get->get('/insurers', 'insurers');
        $get->get('/cities', 'cities');
        $get->get('/countries', 'countries');
        $get->get('/languages', 'languages');
        $get->get('/version', 'version');

        return $get;
    }
}
