<?php
namespace Collections;

use Controllers\PostController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

class PostCollection extends BaseCollection {
    public function registerCollection() {
        $post = new MicroCollection();
        $post->setHandler(new PostController());
        $post->setPrefix('/post');
        $post->post('/requestCode', 'requestCode');
        $post->post('/login', 'login');
        $post->post('/incident', 'incident');

        return $post;
    }
}