<?php
namespace Collections;

abstract class BaseCollection {
    abstract protected function registerCollection();
}