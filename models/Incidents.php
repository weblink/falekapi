<?php

namespace Models;

use Phalcon\Mvc\Model;

class Incidents extends Model {
    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     *
     * @var string
     */
    protected $date;

    /**
     *
     * @var integer
     */
    protected $sent_insurer;

    /**
     *
     * @var integer
     */
    protected $sent_relatives;

    /**
     *
     * @var integer
     */
    protected $threat_of_life;

    /**
     *
     * @var string
     */
    protected $car_plate_number;

    /**
     *
     * @var string
     */
    protected $car_brand;

    /**
     *
     * @var string
     */
    protected $car_model;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id) {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Method to set the value of field date
     *
     * @param string $date
     * @return $this
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Method to set the value of field sent_insurer
     *
     * @param integer $sent_insurer
     * @return $this
     */
    public function setSentInsurer($sent_insurer) {
        $this->sent_insurer = $sent_insurer;

        return $this;
    }

    /**
     * Method to set the value of field sent_relatives
     *
     * @param integer $sent_relatives
     * @return $this
     */
    public function setSentRelatives($sent_relatives) {
        $this->sent_relatives = $sent_relatives;

        return $this;
    }

    /**
     * Method to set the value of field threat_of_life
     *
     * @param integer $threat_of_life
     * @return $this
     */
    public function setThreatOfLife($threat_of_life) {
        $this->threat_of_life = $threat_of_life;

        return $this;
    }

    /**
     * Method to set the value of field car_plate_number
     *
     * @param string $car_plate_number
     * @return $this
     */
    public function setCarPlateNumber($car_plate_number) {
        $this->car_plate_number = $car_plate_number;

        return $this;
    }

    /**
     * Method to set the value of field car_brand
     *
     * @param string $car_brand
     * @return $this
     */
    public function setCarBrand($car_brand) {
        $this->car_brand = $car_brand;

        return $this;
    }

    /**
     * Method to set the value of field car_model
     *
     * @param string $car_model
     * @return $this
     */
    public function setCarModel($car_model) {
        $this->car_model = $car_model;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * Returns the value of field date
     *
     * @return string
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Returns the value of field sent_insurer
     *
     * @return integer
     */
    public function getSentInsurer() {
        return $this->sent_insurer;
    }

    /**
     * Returns the value of field sent_relatives
     *
     * @return integer
     */
    public function getSentRelatives() {
        return $this->sent_relatives;
    }

    /**
     * Returns the value of field threat_of_life
     *
     * @return integer
     */
    public function getThreatOfLife() {
        return $this->threat_of_life;
    }

    /**
     * Returns the value of field car_plate_number
     *
     * @return string
     */
    public function getCarPlateNumber() {
        return $this->car_plate_number;
    }

    /**
     * Returns the value of field car_brand
     *
     * @return string
     */
    public function getCarBrand() {
        return $this->car_brand;
    }

    /**
     * Returns the value of field car_model
     *
     * @return string
     */
    public function getCarModel() {
        return $this->car_model;
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->belongsTo('user_id', 'Models\Users', 'id', array('alias' => 'Users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'incidents';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Incidents[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Incidents
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

}
