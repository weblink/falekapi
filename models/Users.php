<?php

namespace Models;

use Phalcon\Mvc\Model\Validator\Email as Email;
use Phalcon\Mvc\Model;

class Users extends Model {

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $lastname;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     *
     * @var string
     */
    protected $phone_number;

    /**
     *
     * @var string
     */
    protected $car_plate_number;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $important_data;

    /**
     *
     * @var string
     */
    protected $city;

    /**
     *
     * @var string
     */
    protected $post_code;

    /**
     *
     * @var string
     */
    protected $country;

    /**
     *
     * @var string
     */
    protected $street;

    /**
     *
     * @var integer
     */
    protected $inform_relatives;

    /**
     *
     * @var integer
     */
    protected $inform_insurer;

    /**
     *
     * @var integer
     */
    protected $insurer_id;

    /**
     *
     * @var string
     */
    protected $policy_number;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field lastname
     *
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Method to set the value of field phone_number
     *
     * @param string $phone_number
     * @return $this
     */
    public function setPhoneNumber($phone_number) {
        $this->phone_number = $phone_number;

        return $this;
    }

    /**
     * Method to set the value of field car_plate_number
     *
     * @param string $car_plate_number
     * @return $this
     */
    public function setCarPlateNumber($car_plate_number) {
        $this->car_plate_number = $car_plate_number;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field important_data
     *
     * @param string $important_data
     * @return $this
     */
    public function setImportantData($important_data) {
        $this->important_data = $important_data;

        return $this;
    }

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Method to set the value of field post_code
     *
     * @param string $post_code
     * @return $this
     */
    public function setPostCode($post_code) {
        $this->post_code = $post_code;

        return $this;
    }

    /**
     * Method to set the value of field country
     *
     * @param string $country
     * @return $this
     */
    public function setCountry($country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Method to set the value of field street
     *
     * @param string $street
     * @return $this
     */
    public function setStreet($street) {
        $this->street = $street;

        return $this;
    }

    /**
     * Method to set the value of field inform_relatives
     *
     * @param integer $inform_relatives
     * @return $this
     */
    public function setInformRelatives($inform_relatives) {
        $this->inform_relatives = $inform_relatives;

        return $this;
    }

    /**
     * Method to set the value of field inform_insurer
     *
     * @param integer $inform_insurer
     * @return $this
     */
    public function setInformInsurer($inform_insurer) {
        $this->inform_insurer = $inform_insurer;

        return $this;
    }

    /**
     * Method to set the value of field insurer_id
     *
     * @param integer $insurer_id
     * @return $this
     */
    public function setInsurerId($insurer_id) {
        $this->insurer_id = $insurer_id;

        return $this;
    }

    /**
     * Method to set the value of field policy_number
     *
     * @param string $policy_number
     * @return $this
     */
    public function setPolicyNumber($policy_number) {
        $this->policy_number = $policy_number;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns the value of field lastname
     *
     * @return string
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Returns the value of field phone_number
     *
     * @return string
     */
    public function getPhoneNumber() {
        return $this->phone_number;
    }

    /**
     * Returns the value of field car_plate_number
     *
     * @return string
     */
    public function getCarPlateNumber() {
        return $this->car_plate_number;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Returns the value of field important_data
     *
     * @return string
     */
    public function getImportantData() {
        return $this->important_data;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Returns the value of field post_code
     *
     * @return string
     */
    public function getPostCode() {
        return $this->post_code;
    }

    /**
     * Returns the value of field country
     *
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Returns the value of field street
     *
     * @return string
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * Returns the value of field inform_relatives
     *
     * @return integer
     */
    public function getInformRelatives() {
        return $this->inform_relatives;
    }

    /**
     * Returns the value of field inform_insurer
     *
     * @return integer
     */
    public function getInformInsurer() {
        return $this->inform_insurer;
    }

    /**
     * Returns the value of field insurer_id
     *
     * @return integer
     */
    public function getInsurerId() {
        return $this->insurer_id;
    }

    /**
     * Returns the value of field policy_number
     *
     * @return string
     */
    public function getPolicyNumber() {
        return $this->policy_number;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation() {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->hasMany('id', 'Models\Incidents', 'user_id', array('alias' => 'Incidents'));
        $this->hasMany('id', 'Models\Relatives', 'user_id', array('alias' => 'Relatives'));
        $this->belongsTo('insurer_id', 'Models\Insurers', 'id', array('alias' => 'Insurers'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'users';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Users[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Users
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }
}
