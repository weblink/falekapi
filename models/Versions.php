<?php

namespace Models;

use Phalcon\Mvc\Model;

class Versions extends Model {
    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $api;

    /**
     *
     * @var string
     */
    protected $cities;

    /**
     *
     * @var string
     */
    protected $countries;

    /**
     *
     * @var string
     */
    protected $languages;

    /**
     *
     * @var string
     */
    protected $insurers;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field api
     *
     * @param string $api
     * @return $this
     */
    public function setApi($api) {
        $this->api = $api;

        return $this;
    }

    /**
     * Method to set the value of field cities
     *
     * @param string $cities
     * @return $this
     */
    public function setCities($cities) {
        $this->cities = $cities;

        return $this;
    }

    /**
     * Method to set the value of field countries
     *
     * @param string $countries
     * @return $this
     */
    public function setCountries($countries) {
        $this->countries = $countries;

        return $this;
    }

    /**
     * Method to set the value of field languages
     *
     * @param string $languages
     * @return $this
     */
    public function setLanguages($languages) {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Method to set the value of field insurers
     *
     * @param string $insurers
     * @return $this
     */
    public function setInsurers($insurers) {
        $this->insurers = $insurers;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field api
     *
     * @return string
     */
    public function getApi() {
        return $this->api;
    }

    /**
     * Returns the value of field cities
     *
     * @return string
     */
    public function getCities() {
        return $this->cities;
    }

    /**
     * Returns the value of field countries
     *
     * @return string
     */
    public function getCountries() {
        return $this->countries;
    }

    /**
     * Returns the value of field languages
     *
     * @return string
     */
    public function getLanguages() {
        return $this->languages;
    }

    /**
     * Returns the value of field insurers
     *
     * @return string
     */
    public function getInsurers() {
        return $this->insurers;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'versions';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Versions[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Versions
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

}
