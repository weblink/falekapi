<?php

namespace Models;

use Phalcon\Mvc\Model;

class RequestCodes extends Model {

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $phone_number;

    /**
     *
     * @var integer
     */
    protected $pin;

    /**
     *
     * @var string
     */
    protected $expired_time;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field phone_number
     *
     * @param string $phone_number
     * @return $this
     */
    public function setPhoneNumber($phone_number) {
        $this->phone_number = $phone_number;

        return $this;
    }

    /**
     * Method to set the value of field pin
     *
     * @param integer $pin
     * @return $this
     */
    public function setPin($pin) {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Method to set the value of field expired_time
     *
     * @param string $expired_time
     * @return $this
     */
    public function setExpiredTime($expired_time) {
        $this->expired_time = $expired_time;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field phone_number
     *
     * @return string
     */
    public function getPhoneNumber() {
        return $this->phone_number;
    }

    /**
     * Returns the value of field pin
     *
     * @return integer
     */
    public function getPin() {
        return $this->pin;
    }

    /**
     * Returns the value of field expired_time
     *
     * @return string
     */
    public function getExpiredTime() {
        return $this->expired_time;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'request_codes';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestCodes[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestCodes
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

}
