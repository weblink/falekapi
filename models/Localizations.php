<?php

namespace Models;

use Phalcon\Mvc\Model;

class Localizations extends Model {
    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $accuracy;

    /**
     *
     * @var string
     */
    protected $latitude;

    /**
     *
     * @var string
     */
    protected $longitude;

    /**
     *
     * @var string
     */
    protected $provider;

    /**
     *
     * @var string
     */
    protected $date;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field accuracy
     *
     * @param integer $accuracy
     * @return $this
     */
    public function setAccuracy($accuracy) {
        $this->accuracy = $accuracy;

        return $this;
    }

    /**
     * Method to set the value of field latitude
     *
     * @param string $latitude
     * @return $this
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Method to set the value of field longitude
     *
     * @param string $longitude
     * @return $this
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Method to set the value of field provider
     *
     * @param string $provider
     * @return $this
     */
    public function setProvider($provider) {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Method to set the value of field date
     *
     * @param string $date
     * @return $this
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field accuracy
     *
     * @return integer
     */
    public function getAccuracy() {
        return $this->accuracy;
    }

    /**
     * Returns the value of field latitude
     *
     * @return string
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Returns the value of field longitude
     *
     * @return string
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * Returns the value of field provider
     *
     * @return string
     */
    public function getProvider() {
        return $this->provider;
    }

    /**
     * Returns the value of field date
     *
     * @return string
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'localizations';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Localizations[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Localizations
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

}
