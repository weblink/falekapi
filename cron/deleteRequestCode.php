<?php
# */5 * * * * /usr/bin/php /var/www/falek/cron/deleteRequestCodes.php ----- cron uruchamiany co 5 minut (crontab -e)

$config = include __DIR__.'/../config/config.php';

include __DIR__. '/../config/services.php';

include __DIR__. '/../config/loader.php';

use Models\RequestCodes;

RequestCodes::find(array(
    'conditions' => 'expired_time < :date:',
    'bind' => array(
        'date' => date("Y-m-d H:i:s", time())
    )
))->delete();


